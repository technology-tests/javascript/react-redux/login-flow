import React from "react"

import { Grid } from "semantic-ui-react"

export const CenterContainer = (props) => (
  <Grid
    textAlign="center"
    style={{ fontSize: "16px", height: "100vh", margin: 0 }}
    verticalAlign="middle"
  >
    <Grid.Column
      textAlign="left"
      style={{
        maxHeight: "100vh",
        minHeight: "360px",
        padding: "1rem",
        width: "640px",
      }}
    >
      {props.children}
    </Grid.Column>
  </Grid>
)
