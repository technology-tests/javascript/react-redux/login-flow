module.exports = {
  settings: {
    react: {
      version: "detect",
    },
  },
  env: {
    browser: true,
    node: false,
    es6: true,
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: "latest",
    sourceType: "module",
  },
  rules: {
    "react/no-unescaped-entities": "off",
    "react/prop-types": "off",
  },
  extends: ["eslint:recommended", "plugin:react/recommended"],
  overrides: [
    {
      files: "./**/*.{js,jsx,ts,tsx}",
    },
  ],
}
