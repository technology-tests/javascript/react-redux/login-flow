import axios from "axios"

export const BASE_URL = new URL(
  import.meta.env.VITE_BASE_URL || "http://localhost:3000"
)

export const axiosApi = axios.create({
  baseURL: BASE_URL.toString(),
})

export const Status = {
  FULFILLED: "fulfilled",
  PENDING: "pending",
  READY: "ready",
  REJECTED: "rejected",
}

export const THEME_COLOR = "teal"
