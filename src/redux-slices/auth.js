import { createAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit"

import { axiosApi, Status } from "/constants"

const initialState = {
  user: null,
  status: {
    type: Status.READY,
    message: null,
  },
}

const isPendingAction = (action) => action.type.endsWith("/pending")
const isFulfilledAction = (action) => action.type.endsWith("/fulfilled")
const isRejectedAction = (action) => action.type.endsWith("/rejected")

const userLoginAction = createAction("user/login")

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    logout: () => initialState,
  },
  extraReducers: (builder) => {
    builder
      .addCase(userLoginAction, () => {})
      .addMatcher(isPendingAction, (state) => {
        state.status = {
          ...state.status,
          type: Status.PENDING,
        }
      })
      .addMatcher(isFulfilledAction, (state, { payload }) => {
        state.user = payload.user
        state.status = {
          ...state.status,
          type: Status.FULFILLED,
          message: null,
        }
      })
      .addMatcher(isRejectedAction, (state, { payload }) => {
        state.status = {
          ...state.status,
          type: Status.REJECTED,
          message: payload.message,
        }
      })
      .addDefaultCase(() => {})
  },
})

export const userLogin = createAsyncThunk(
  userLoginAction.type,
  async (credentials, { rejectWithValue }) => {
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    }

    return axiosApi
      .post(`/api/user/login`, credentials, config)
      .then((response) => response.data)
      .catch((error) => {
        const message = error.response?.data ?? error
        return rejectWithValue(message)
      })
  }
)

export const selectAuth = (state) => state.auth
export const { logout } = authSlice.actions

export default authSlice.reducer
