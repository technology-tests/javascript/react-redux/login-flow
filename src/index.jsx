import React from "react"
import ReactDOM from "react-dom/client"

import { Provider } from "react-redux"
import { RouterProvider } from "react-router-dom"
import { PersistGate } from "redux-persist/integration/react"
import "semantic-ui-css/semantic.min.css"

import { enableFakeServer } from "/fake-server"
import { router } from "/routes"
import { persistor, store } from "/store"

enableFakeServer()

const root = ReactDOM.createRoot(document.getElementById("root"))
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <RouterProvider router={router} />
      </PersistGate>
    </Provider>
  </React.StrictMode>
)
