import React from "react"

import { Link } from "react-router-dom"
import { Button, Header, Message, Segment } from "semantic-ui-react"

import { CenterContainer } from "/components/CenterContainer"
import { THEME_COLOR } from "/constants"

export const ErrorPage = () => (
  <CenterContainer>
    <Header color={THEME_COLOR} textAlign="center">
      Page not found
    </Header>
    <Segment>
      <Message
        error
        header="Unable to find requested page"
        content="The page you requested could not be found."
      />

      <Link to="/">
        <Button color={THEME_COLOR} fluid>
          Return to homepage
        </Button>
      </Link>
    </Segment>
  </CenterContainer>
)
