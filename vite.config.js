import legacy from "@vitejs/plugin-legacy"
import { defineConfig } from "vite"
import eslint from "vite-plugin-eslint"

export default defineConfig({
  base: "./",
  plugins: [eslint(), legacy()],
})
