import React from "react"

import { useSelector } from "react-redux"
import { Navigate } from "react-router-dom"

import { selectAuth } from "/redux-slices/auth"

export const PrivateRoute = ({ component: Component }) => {
  const auth = useSelector((state) => selectAuth(state))
  const isLoggedIn = Boolean(auth.user)

  return isLoggedIn ? <Component /> : <Navigate to={{ pathname: "/" }} />
}
