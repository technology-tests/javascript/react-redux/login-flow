import React from "react"

import { useDispatch, useSelector } from "react-redux"
import { useNavigate } from "react-router-dom"
import { Button, Divider, Header, Message, Segment } from "semantic-ui-react"

import { CenterContainer } from "/components/CenterContainer"
import { THEME_COLOR } from "/constants"
import { logout, selectAuth } from "/redux-slices/auth"

export const DashboardPage = () => {
  const auth = useSelector((state) => selectAuth(state))

  const dispatch = useDispatch()
  const navigate = useNavigate()

  const onclickLogoutButton = () => {
    dispatch(logout())
    navigate("/")
  }

  const lastLogin = new Date(auth.user.lastLogin)
  const dateOptions = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
    hour: "numeric",
    minute: "numeric",
    second: "numeric",
    timeZoneName: "short",
  }
  const lastLoginMessage = auth.user.lastLogin
    ? `You last logged in at ${lastLogin.toLocaleString(
        undefined,
        dateOptions
      )}.`
    : "This is your first time logging in."

  return (
    <CenterContainer>
      <Header color={THEME_COLOR} textAlign="center">
        Welcome @{auth.user.username}!
      </Header>
      <Segment>
        <Message info content={lastLoginMessage} />

        <div>
          To reset login activity:
          <ol>
            <li>use a new private browsing session</li>
          </ol>
          or
          <ol>
            <li>
              run <code>localStorage.clear()</code> in your browser's developer
              console
            </li>
            <li>refresh the page</li>
          </ol>
        </div>

        <Button
          color={THEME_COLOR}
          fluid
          autoFocus
          onClick={onclickLogoutButton}
        >
          Log out
        </Button>

        <Divider />

        <p>
          <a href="https://gitlab.com/technology-tests/javascript/react-redux/login-flow">
            View this website's source code.
          </a>
        </p>
      </Segment>
    </CenterContainer>
  )
}
