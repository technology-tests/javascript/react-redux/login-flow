# Redux Login Flow

## Live demo

- https://technology-tests.gitlab.io/javascript/react-redux/login-flow

### Run locally

```sh
git clone 'https://gitlab.com/technology-tests/javascript/react-redux/login-flow.git' demo
cd demo
sudo docker-compose up --build

# Open http://localhost:3000
```


## Things to try in this demo

- logging in with correct password
    - username and password are the same (for example: `guest` as username and password)
- logging in with incorrect password
    - username and password are not the same (`guest` user with a password other than `guest`)
- logging in with disabled account
    - username of `admin`, `administrator`, and `root`
- when logged out, navigate to `/dashboard` (private route) directly
    - should redirect to login page automatically
- when logged in, navigate to `/` (login page) directly
    - should redirect to dashboard automatically
- when logged in and out, navigate to `/reset-password` directly
    - should not redirect anywhere, as it is available to both logged in and out users


## Frontend

- Redux Toolkit, to manage and centralize application state
- React Router, for client-siding routing as a single-page application


## Backend

- the backend API is faked using [Mirage JS][Mirage JS] in the frontend
  so there are no network requests
    - the frontend code does not need to be modified except disabling the faked server
      when deploying to production (all `fetch` and `axios.get(...)` requests will no longer be intercepted)
- the whole demo is hosted on GitLab Pages as a static site


[Mirage JS]: https://miragejs.com/
