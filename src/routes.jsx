import React from "react"

import { createBrowserRouter } from "react-router-dom"

import { PrivateRoute } from "/components/PrivateRoute"
import { BASE_URL } from "/constants"
import { DashboardPage } from "/pages/Dashboard"
import { ErrorPage } from "/pages/Error"
import { LoginPage } from "/pages/Login"
import { ResetPasswordPage } from "/pages/ResetPassword"

const routes = [
  {
    path: "/",
    element: <LoginPage />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/dashboard",
    element: <PrivateRoute component={DashboardPage} />,
  },
  {
    path: "/reset-password",
    element: <ResetPasswordPage />,
  },
]

const config = {
  basename: BASE_URL.pathname,
}

export const router = createBrowserRouter(routes, config)
