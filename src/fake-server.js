import { createServer, Response } from "miragejs"

import { BASE_URL } from "/constants"

const getLastLogin = () => {
  const saveData = JSON.parse(localStorage.getItem("lastLogin"))
  return new Map(saveData)
}

const lastLogin = getLastLogin()

const saveLastLogin = () => {
  const saveData = JSON.stringify([...lastLogin])
  localStorage.setItem("lastLogin", saveData)
}

export const enableFakeServer = () => {
  createServer({
    routes: function () {
      this.logging = true
      this.timing = 250

      this.namespace = "/api"
      this.urlPrefix = BASE_URL.toString()

      // Note: although Mirage JS has a built-in ORM,
      // it is simpler ,and sufficient for this demo,
      // to generate and return the response as below
      this.post("/user/login", (schema, request) => {
        const data = JSON.parse(request.requestBody)

        if (["admin", "administrator", "root"].includes(data.username)) {
          const response = {
            status: 401,
            headers: {},
            data: {
              message: "account disabled (use a non-admin account)",
            },
          }

          return new Response(response.status, response.headers, response.data)
        }

        if (data.username !== data.password) {
          const response = {
            status: 401,
            headers: {},
            data: {
              message:
                "incorrect password (set password equal to username to log in)",
            },
          }

          return new Response(response.status, response.headers, response.data)
        }

        // Note: a production-ready server should return the account's ID too
        const response = {
          status: 200,
          headers: {},
          data: {
            user: {
              username: data.username,
              lastLogin: lastLogin.get(data.username),
            },
          },
        }

        lastLogin.set(data.username, new Date())
        saveLastLogin()

        return new Response(response.status, response.headers, response.data)
      })

      // A catch-all for all unhandled requests
      // Eases debugging as unhandled requests are not logged,
      // and do not show up in the network monitor
      // (since no network requests are made)
      const unregisteredRouteHandler = () => {
        const response = {
          status: 400,
          headers: {},
          data: {
            message: "unregistered route",
          },
        }

        return new Response(response.status, response.headers, response.data)
      }

      const httpVerbs = [
        this.get,
        this.post,
        this.patch,
        this.put,
        this.del,
        this.options,
      ]

      httpVerbs.forEach((func) => {
        const args = ["**", unregisteredRouteHandler]
        func.apply(this, args)
      })
    },
  })
}
