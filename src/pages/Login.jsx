import React, { useEffect, useState } from "react"

import { useDispatch, useSelector } from "react-redux"
import { Link, useNavigate } from "react-router-dom"
import {
  Button,
  Divider,
  Dropdown,
  Form,
  Header,
  Message,
  Segment,
} from "semantic-ui-react"

import { CenterContainer } from "/components/CenterContainer"
import { Status, THEME_COLOR } from "/constants"
import { selectAuth, userLogin } from "/redux-slices/auth"

export const LoginPage = () => {
  const auth = useSelector((state) => selectAuth(state))

  const dispatch = useDispatch()
  const navigate = useNavigate()

  const [username, setUsername] = useState("guest")
  const [password, setPassword] = useState(username)

  const pendingLogin = auth.status.type === Status.PENDING

  const prefillOptions = [
    { key: "guest", text: "as guest", value: "guest" },
    { key: "administrator", text: "as admin", value: "administrator" },
  ]

  const handleOnPrefillChange = (event, data) => {
    setUsername(data.value)
    setPassword(data.value)
  }

  const handleLogin = (event) => {
    const { target } = event
    const data = Object.fromEntries(new FormData(target))

    dispatch(userLogin(data))
  }

  useEffect(() => {
    if (auth.user?.username) {
      navigate("/dashboard")
    }
  }, [auth.user])

  return (
    <CenterContainer>
      <Header color={THEME_COLOR} textAlign="center">
        Log in to your account
      </Header>
      <Segment>
        {(auth.status.type === Status.REJECTED ||
          (pendingLogin && auth.status.message)) && (
          <Message
            error
            header="Unable to login"
            list={[auth.status.message]}
          />
        )}
        <Form onSubmit={handleLogin}>
          <Form.Input
            autoFocus
            value={username}
            onChange={(event, data) => setUsername(data.value)}
            fluid
            icon="user"
            iconPosition="left"
            name="username"
            placeholder="username"
            action={
              <Dropdown
                button
                options={prefillOptions}
                onChange={handleOnPrefillChange}
                defaultValue="guest"
              />
            }
          />
          <Form.Input
            value={password}
            onChange={(event, data) => setPassword(data.value)}
            fluid
            icon="lock"
            iconPosition="left"
            name="password"
            placeholder="password"
            type="password"
          />

          <Button
            color={THEME_COLOR}
            fluid
            loading={pendingLogin}
            disabled={pendingLogin || !username || !password}
          >
            Log in
          </Button>
        </Form>

        <Divider />

        <p>
          <Link to="/reset-password">Forgot password?</Link>
        </p>
      </Segment>
    </CenterContainer>
  )
}
