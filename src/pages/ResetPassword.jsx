import React from "react"

import { Link } from "react-router-dom"
import { Button, Header, Message, Segment } from "semantic-ui-react"

import { CenterContainer } from "/components/CenterContainer"
import { THEME_COLOR } from "/constants"

export const ResetPasswordPage = () => (
  <CenterContainer>
    <Header color={THEME_COLOR} textAlign="center">
      Reset your password
    </Header>
    <Segment>
      <Message
        error
        header="Password resets disabled"
        content="This feature has been disabled for security reasons."
      />

      <Link to="/">
        <Button color={THEME_COLOR} fluid>
          Return to homepage
        </Button>
      </Link>
    </Segment>
  </CenterContainer>
)
