FROM node:20.4.0

RUN apt-get update && apt-get install -y rsync

WORKDIR /tmp/app

COPY package.json .

RUN npm install

WORKDIR /app

CMD \
    rsync \
        --recursive \
        --archive \
        --checksum \
        --delete \
        --verbose --human-readable \
        --info=progress2 --info=name0 \
        /tmp/app/node_modules/ ./node_modules \
    || exit $?; \
    npm run format; \
    npm run serve
